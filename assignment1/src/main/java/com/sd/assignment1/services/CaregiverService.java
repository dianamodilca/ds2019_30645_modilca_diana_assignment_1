package com.sd.assignment1.services;

import com.sd.assignment1.entities.Caregiver;
import com.sd.assignment1.errorhandler.ResourceNotFoundException;
import com.sd.assignment1.repositories.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;

    public Caregiver findCaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        Caregiver c = new Caregiver();
        c.setUserID(caregiver.get().getUserID());
        c.setPassword(caregiver.get().getPassword());
        c.setUsername(caregiver.get().getUsername());
        c.setRole(caregiver.get().getRole());
        c.setName(caregiver.get().getName());
        c.setGender(caregiver.get().getGender());
        c.setEmail(caregiver.get().getEmail());
        c.setAddress(caregiver.get().getAddress());

        return caregiverRepository.save(c);
    }

    public List<Caregiver> findAll(){
        List<Caregiver> caregivers = caregiverRepository.findAll();

        return caregivers;
    }


    public Integer insert(Caregiver caregiver) {


        Caregiver c = caregiverRepository.save(caregiver);

        return c.getUserID();

    }

    public Integer update(Caregiver caregiver, int id) {

        Optional<Caregiver> car = caregiverRepository.findById(id);
        // Caregiver d = new Caregiver();

        caregiver.setUserID(id);
        caregiverRepository.save(caregiver);
//        d.setPassword(doc.get().getPassword());
//        d.setUsername(doc.get().getUsername());
//        d.setRole(doc.get().getRole());
//        d.setName(doc.get().getName());
//        d.setEmail(doc.get().getEmail());
//        d.setPatientList(doc.get().getPatientList());
//        d.setCaregiverList(doc.get().getCaregiverList());
//        d.setIntakeList(doc.get().getIntakeList());
        //     caregiverRepository.save(d);

        return caregiver.getUserID();
    }

    public void delete(int id){
        this.caregiverRepository.deleteById(id);
    }

}
